package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by SUMIT on 01-11-2017.
 */
public interface UnmeteredUnitInterface extends BeanInterface {

    public long getId() ;

    public void setId(long id) ;

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public long getTariffId() ;

    public void setTariffId(long tariffId);

    public long getBillingUnit();

    public void setBillingUnit(long billingUnit);
}
