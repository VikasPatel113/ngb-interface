package com.mppkvvcl.ngbinterface.interfaces;
import java.math.BigDecimal;
import java.util.Date;

public interface OnlinePaymentRebateInterface extends BeanInterface{

    public long getId();

    public void setId(long id) ;

    public BigDecimal getMinimum();

    public void setMinimum(BigDecimal minimum) ;

    public BigDecimal getMaximum();

    public void setMaximum(BigDecimal maximum) ;

    public String getRate();

    public void setRate(String rate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier) ;

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

}
