package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface GroupInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getLocationCode();

    public void setLocationCode(String locationCode);
}
