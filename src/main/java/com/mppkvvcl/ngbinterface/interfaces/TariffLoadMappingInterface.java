package com.mppkvvcl.ngbinterface.interfaces;

public interface TariffLoadMappingInterface extends BeanInterface {

    public static boolean IS_TARIFF_CHANGE_TRUE = true;

    public static boolean IS_TARIFF_CHANGE_FALSE = false;

    public static boolean IS_LOAD_CHANGE_TRUE = true;

    public static boolean IS_LOAD_CHANGE_FALSE = false;

    public long getId();

    public void setId(long id);

    public long getTariffDetailId();

    public void setTariffDetailId(long tariffDetailId);

    public long getLoadDetailId();

    public void setLoadDetailId(long loadDetailId);

    public boolean isTariffChange();

    public void setTariffChange(boolean tariffChange);

    public boolean isLoadChange();

    public void setLoadChange(boolean loadChange);

}
