package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerConnectionPaymentInformationInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public Date getDateOfRegistration();

    public void setDateOfRegistration(Date dateOfRegistration);

    public BigDecimal getRegistrationFeeAmount();

    public void setRegistrationFeeAmount(BigDecimal registrationFeeAmount);

    public String getRegistrationFeeAmountMRNo();

    public void setRegistrationFeeAmountMRNo(String registrationFeeAmountMRNo);

    public BigDecimal getSecurityDepositAmount();

    public void setSecurityDepositAmount(BigDecimal securityDepositAmount);

    public String getSecurityDepositAmountMRNo();

    public void setSecurityDepositAmountMRNo(String securityDepositAmountMRNo);

    public void setCreatedOn(Date createdOn);

    public Date getCreatedOn();

    public void setCreatedBy(String createdBy);

    public String getCreatedBy();

    public void setUpdatedOn(Date updatedOn);

    public Date getUpdatedOn();

    public void setUpdatedBy(String updatedBy);

    public String getUpdatedBy();

}
