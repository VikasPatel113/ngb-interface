package com.mppkvvcl.ngbinterface.interfaces;

import java.util.Date;

public interface TariffChangeDetailInterface extends BeanInterface {

    public long getId() ;

    public void setId(long id);

    public long getTariffDetailId() ;

    public void setTariffDetailId(long tariffDetailId);

    public long getPurposeOfInstallationId();

    public void setPurposeOfInstallationId(long purposeOfInstallationId) ;

    public String getPurposeOfInstallation() ;

    public void setPurposeOfInstallation(String purposeOfInstallation);

    public String getMeteringStatus() ;

    public void setMeteringStatus(String meteringStatus) ;

    public String getCreatedBy() ;

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn) ;

    public String getUpdatedBy() ;

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn() ;

    public void setUpdatedOn(Date updatedOn);

    public boolean getIsSeasonal() ;

    public void setIsSeasonal(boolean isSeasonal) ;

    public boolean getIsXray();

    public void setIsXray(boolean xray) ;

}
