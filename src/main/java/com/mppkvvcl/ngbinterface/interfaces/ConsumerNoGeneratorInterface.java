package com.mppkvvcl.ngbinterface.interfaces;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerNoGeneratorInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public String getAppPrefix();

    public void setAppPrefix(String appPrefix);

    public String getDiscomCode();

    public void setDiscomCode(String discomCode);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getTrailingCode();

    public void setTrailingCode(String trailingCode);

    public long getLatestConsumerNo();

    public void setLatestConsumerNo(long latestConsumerNo);

    public long getTotalConsumerNoGenerated();

    public void setTotalConsumerNoGenerated(long totalConsumerNoGenerated);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public String getUpdatedBy() ;

    public void setUpdatedBy(String updatedBy);
}
