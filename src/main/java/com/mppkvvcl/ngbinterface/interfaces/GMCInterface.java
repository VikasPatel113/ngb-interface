package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface GMCInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getSubCategoryCode();

    public void setSubCategoryCode(long subCategoryCode);

    public String getLoadUnit();

    public void setLoadUnit(String loadUnit);

    public BigDecimal getMinimum();

    public void setMinimum(BigDecimal minimum);

    public long getTariffId() ;

    public void setTariffId(long tariffId);
}
