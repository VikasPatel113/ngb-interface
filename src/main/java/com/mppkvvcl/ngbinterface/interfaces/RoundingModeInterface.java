package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface RoundingModeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public int getCode();

    public void setCode(int code);

    public String getType();

    public void setType(String type);

    public String getDescription();

    public void setDescription(String description);
}
