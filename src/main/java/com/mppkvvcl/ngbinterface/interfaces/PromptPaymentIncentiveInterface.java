package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

public interface PromptPaymentIncentiveInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public BigDecimal getMinimumAmount();

    public void setMinimumAmount(BigDecimal minimumAmount);

    public BigDecimal getMaximumAmount();

    public void setMaximumAmount(BigDecimal maximumAmount);

    public String getRate();

    public void setRate(String rate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

    public int getDay();

    public void setDay(int day);
}
