package com.mppkvvcl.ngbinterface.interfaces;

public interface BillTypeCodeInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public String getCode();

    public void setCode(String code);

    public String getDescription();

    public void setDescription(String description);
}
