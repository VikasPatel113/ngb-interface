package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ZoneInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public String getCode();

    public void setCode(String code);

    public String getName();

    public void setName(String name);

    public String getShortCode() ;

    public void setShortCode(String shortCode) ;

    public String getAddress();

    public void setAddress(String address);

    public String getPhoneNo();

    public void setPhoneNo(String phoneNo);

    public DivisionInterface getDivision();

    public void setDivision(DivisionInterface divisionInterface);
}
