package com.mppkvvcl.ngbinterface.interfaces;

public interface ExcessDemandSubcategoryMappingInterface extends BeanInterface {
    public long getId() ;

    public void setId(long id) ;

    public long getTariffId() ;

    public void setTariffId(long tariffId);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public long getExcessDemandConfigurationId() ;

    public void setExcessDemandConfigurationId(long excessDemandConfigurationId);

    public ExcessDemandConfigurationInterface getExcessDemandConfiguration() ;

    public void setExcessDemandConfiguration(ExcessDemandConfigurationInterface excessDemandConfiguration) ;

}
