package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ErrorMessageInterface extends BeanInterface {

    public String getErrorMessage();

    public void setErrorMessage(String errorMessage);

}
