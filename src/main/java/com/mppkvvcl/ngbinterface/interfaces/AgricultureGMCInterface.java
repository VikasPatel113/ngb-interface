package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

public interface AgricultureGMCInterface extends BeanInterface{
    public long getId();

    public void setId(long id) ;

    public long getTariffId() ;

    public void setTariffId(long tariffId);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode) ;

    public String getStartMonth();

    public void setStartMonth(String startMonth) ;

    public String getEndMonth() ;

    public void setEndMonth(String endMonth) ;

    public BigDecimal getMinimum();

    public void setMinimum(BigDecimal minimum);

    public String getLoadUnit();

    public void setLoadUnit(String loadUnit);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

}
