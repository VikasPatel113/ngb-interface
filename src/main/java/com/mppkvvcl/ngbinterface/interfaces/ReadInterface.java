package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ReadInterface extends BeanInterface {
    public ConsumerInformationInterface getConsumerInformation();

    public void setConsumerInformation(ConsumerInformationInterface consumerInformationInterface);

    public BigDecimal getMf();

    public void setMf(BigDecimal mf);

    public ScheduleInterface getSchedule();

    public void setSchedule(ScheduleInterface scheduleInterface);

    public ReadMasterInterface getReadMaster();

    public void setReadMaster(ReadMasterInterface readMasterInterface);

    public ReadTypeConfiguratorInterface getReadTypeConfigurator();

    public void setReadTypeConfigurator(ReadTypeConfiguratorInterface readTypeConfigurator);

}
