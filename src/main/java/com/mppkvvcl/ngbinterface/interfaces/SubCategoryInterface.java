package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 22-09-2017.
 * Latest by Nitish on 23-10-2017
 */
public interface SubCategoryInterface extends BeanInterface {

    public static final String PREMISE_TYPE_RURAL = "RURAL";

    public static final String PREMISE_TYPE_URBAN = "URBAN";

    public long getId();

    public void setId(long id);

    public long getTariffId();

    public void setTariffId(long tariffId);

    public long getCode();

    public void setCode(long code);

    public String getSlabId();

    public void setSlabId(String slabId);

    public String getDescription();

    public void setDescription(String description);

    public BigDecimal getStartContractDemandKW();

    public void setStartContractDemandKW(BigDecimal startContractDemandKW);

    public BigDecimal getEndContractDemandKW();

    public void setEndContractDemandKW(BigDecimal endContractDemandKW);

    public BigDecimal getStartConnectedLoadKW();

    public void setStartConnectedLoadKW(BigDecimal startConnectedLoadKW);

    public BigDecimal getEndConnectedLoadKW();

    public void setEndConnectedLoadKW(BigDecimal endConnectedLoadKW);

    public String getPremiseType();

    public void setPremiseType(String premiseType);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getApplicantType();

    public void setApplicantType(String applicantType);

    public List<SurchargeInterface> getSurchargeInterfaces();

    public void setSurchargeInterfaces(List<SurchargeInterface> surchargeInterfaces);

    public List<SlabInterface> getSlabInterfaces();

    public void setSlabInterfaces(List<SlabInterface> slabInterfaces);

    public FCAInterface getFcaInterface();

    public void setFcaInterface(FCAInterface fcaInterface);

    public List<FCAInterface> getFcaInterfaces() ;

    public void setFcaInterfaces(List<FCAInterface> fcaInterfaces) ;

    public PowerFactorInterface getPowerFactorInterface();

    public void setPowerFactorInterface(PowerFactorInterface powerFactorInterface);

    public List<PowerFactorInterface> getPowerFactorInterfaces();

    public void setPowerFactorInterfaces(List<PowerFactorInterface> powerFactorInterfaces);

    public WeldingSurchargeConfigurationInterface getWeldingSurchargeConfigurationInterface();

    public void setWeldingSurchargeConfigurationInterface(WeldingSurchargeConfigurationInterface weldingSurchargeConfigurationInterface);

    public List<LoadFactorIncentiveConfigurationInterface> getLoadFactorIncentiveConfigurationInterfaces();

    public void setLoadFactorIncentiveConfigurationInterfaces(List<LoadFactorIncentiveConfigurationInterface> loadFactorIncentiveConfigurationInterfaces);

    public IrrigationSchemeInterface getIrrigationSchemeInterface();

    public void setIrrigationSchemeInterface(IrrigationSchemeInterface irrigationSchemeInterface);

    public AgricultureUnitInterface getAgricultureUnitInterface();

    public void setAgricultureUnitInterface(AgricultureUnitInterface agricultureUnitInterface);

    public List<SlabInterface> getAlternateSlabInterfaces();

    public void setAlternateSlabInterfaces(List<SlabInterface> alternateSlabInterfaces);

    public MeterRentInterface getMeterRentInterface();

    public void setMeterRentInterface(MeterRentInterface meterRentInterface);

    public List<ElectricityDutyInterface> getElectricityDutyInterfaces();

    public void setElectricityDutyInterfaces(List<ElectricityDutyInterface> electricityDutyInterfaces);

    public void setMinimumChargeInterface(MinimumChargeInterface minimumChargeInterface);

    public MinimumChargeInterface getMinimumChargeInterface();

    public GMCInterface getGmcInterface() ;

    public void setGmcInterface(GMCInterface gmcInterface) ;

    public AgricultureGMCInterface getAgricultureGMCInterface();

    public void setAgricultureGMCInterface(AgricultureGMCInterface agricultureGMCInterface);

    public List<ExcessDemandSubcategoryMappingInterface> getExcessDemandSubcategoryMappingInterfaces();

    public void setExcessDemandSubcategoryMappingInterfaces(List<ExcessDemandSubcategoryMappingInterface> excessDemandSubcategoryMappingInterfaces);

    public List<XrayConnectionFixedChargeRateInterface> getXrayConnectionFixedChargeRateInterfaces() ;

    public void setXrayConnectionFixedChargeRateInterfaces(List<XrayConnectionFixedChargeRateInterface> xrayConnectionFixedChargeRateInterfaces) ;

    public XrayConnectionInformationInterface getXrayConnectionInformationInterface();

    public void setXrayConnectionInformationInterface(XrayConnectionInformationInterface xrayConnectionInformationInterface);

    public UnmeteredUnitInterface getUnmeteredUnitInterface();

    public void setUnmeteredUnitInterface(UnmeteredUnitInterface unmeteredUnitInterface);

    public List<SubsidyInterface> getSubsidyInterfaces();

    public void setSubsidyInterfaces(List<SubsidyInterface> subsidyInterfaces);

    public List<PurposeSubsidyMappingInterface> getPurposeSubsidyMappingInterfaces();

    public void setPurposeSubsidyMappingInterfaces(List<PurposeSubsidyMappingInterface> purposeSubsidyMappingInterfaces);

    public EmployeeRebateInterface getEmployeeRebateInterface();

    public void setEmployeeRebateInterface(EmployeeRebateInterface employeeRebateInterface);
}
