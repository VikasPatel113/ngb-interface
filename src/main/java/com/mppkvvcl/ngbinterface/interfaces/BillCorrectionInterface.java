package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;
/**
 * Created by Vikas on 12/30/2017.
 */
public interface BillCorrectionInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getBillId();

    public void setBillId(long billId);

    public long getReferenceBillId();

    public void setReferenceBillId(long referenceBillId);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getReadingDiaryNo();

    public void setReadingDiaryNo(String readingDiaryNo);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getBillMonth();

    public void setBillMonth(String billMonth);

    public String getBillTypeCode();

    public void setBillTypeCode(String billTypeCode);

    public Date getBillDate();

    public void setBillDate(Date billDate);

    public Date getDueDate();

    public void setDueDate(Date dueDate);

    public Date getChequeDueDate();

    public void setChequeDueDate(Date chequeDueDate);

    public Date getCurrentReadDate();

    public void setCurrentReadDate(Date currentReadDate);

    public BigDecimal getCurrentRead();

    public void setCurrentRead(BigDecimal currentRead);

    public BigDecimal getPreviousRead();

    public void setPreviousRead(BigDecimal previousRead);

    public BigDecimal getDifference();

    public void setDifference(BigDecimal difference);

    public BigDecimal getMf();

    public void setMf(BigDecimal mf);

    public BigDecimal getMeteredUnit();

    public void setMeteredUnit(BigDecimal meteredUnit);

    public BigDecimal getAssessment();

    public void setAssessment(BigDecimal assessment);

    public BigDecimal getTotalUnit();

    public void setTotalUnit(BigDecimal totalUnit);

    public BigDecimal getGmcUnit();

    public void setGmcUnit(BigDecimal gmcUnit);

    public BigDecimal getBilledUnit();

    public void setBilledUnit(BigDecimal billedUnit);

    public BigDecimal getBilledMD();

    public void setBilledMD(BigDecimal billedMD);

    public BigDecimal getBilledPF();

    public void setBilledPF(BigDecimal billedPF);

    public BigDecimal getLoadFactor();

    public void setLoadFactor(BigDecimal loadFactor);

    public BigDecimal getFixedCharge();

    public void setFixedCharge(BigDecimal fixedCharge);

    public BigDecimal getAdditionalFixedCharges1();

    public void setAdditionalFixedCharges1(BigDecimal additionalFixedCharges1);

    public BigDecimal getAdditionalFixedCharges2();

    public void setAdditionalFixedCharges2(BigDecimal additionalFixedCharges2);

    public BigDecimal getEnergyCharge();

    public void setEnergyCharge(BigDecimal energyCharge);

    public BigDecimal getFcaCharge();

    public void setFcaCharge(BigDecimal fcaCharge);

    public BigDecimal getElectricityDuty();

    public void setElectricityDuty(BigDecimal electricityDuty);

    public BigDecimal getMeterRent();

    public void setMeterRent(BigDecimal meterRent);

    public BigDecimal getPfCharge();

    public void setPfCharge(BigDecimal pfCharge);

    public BigDecimal getWeldingTransformerSurcharge();

    public void setWeldingTransformerSurcharge(BigDecimal weldingTransformerSurcharge);

    public BigDecimal getLoadFactorIncentive();

    public void setLoadFactorIncentive(BigDecimal loadFactorIncentive);

    public BigDecimal getSdInterest();

    public void setSdInterest(BigDecimal sdInterest);

    public BigDecimal getCcbAdjustment();

    public void setCcbAdjustment(BigDecimal ccbAdjustment);

    public BigDecimal getLockCredit();

    public void setLockCredit(BigDecimal lockCredit);

    public BigDecimal getOtherAdjustment();

    public void setOtherAdjustment(BigDecimal otherAdjustment);

    public BigDecimal getEmployeeRebate();

    public void setEmployeeRebate(BigDecimal employeeRebate);

    public BigDecimal getOnlinePaymentRebate();

    public void setOnlinePaymentRebate(BigDecimal onlinePaymentRebate);

    public BigDecimal getPrepaidMeterRebate();

    public void setPrepaidMeterRebate(BigDecimal prepaidMeterRebate);

    public BigDecimal getPromptPaymentIncentive();

    public void setPromptPaymentIncentive(BigDecimal promptPaymentIncentive);

    public BigDecimal getAdvancePaymentIncentive();

    public void setAdvancePaymentIncentive(BigDecimal advancePaymentIncentive);

    public BigDecimal getDemandSideIncentive();

    public void setDemandSideIncentive(BigDecimal demandSideIncentive);

    public BigDecimal getSubsidy();

    public void setSubsidy(BigDecimal subsidy);

    public BigDecimal getCurrentBill();

    public void setCurrentBill(BigDecimal currentBill);

    public BigDecimal getArrear();

    public void setArrear(BigDecimal arrear);

    public BigDecimal getCumulativeSurcharge();

    public void setCumulativeSurcharge(BigDecimal cumulativeSurcharge);

    public BigDecimal getSurchargeDemanded();

    public void setSurchargeDemanded(BigDecimal surchargeDemanded);

    public BigDecimal getNetBill();

    public void setNetBill(BigDecimal netBill);

    public BigDecimal getAsdBilled();

    public void setAsdBilled(BigDecimal asdBilled);

    public BigDecimal getAsdArrear();

    public void setAsdArrear(BigDecimal asdArrear);

    public BigDecimal getAsdInstallment();

    public void setAsdInstallment(BigDecimal asdInstallment);

    public boolean isDeleted();

    public void setDeleted(boolean deleted);

    public void setUpdatedOn(Date updatedOn);

    public BigDecimal getPristineElectricityDuty();

    public void setPristineElectricityDuty(BigDecimal pristineElectricityDuty);

    public BigDecimal getPristineNetBill();

    public void setPristineNetBill(BigDecimal pristineNetBill);

    public BigDecimal getCurrentBillSurcharge();

    public void setCurrentBillSurcharge(BigDecimal currentBillSurcharge);

    public BigDecimal getXrayFixedCharge();

    public void setXrayFixedCharge(BigDecimal xrayFixedCharge);

    public boolean isUsedOnMis();

    public void setUsedOnMis(boolean usedOnMis);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();
}
